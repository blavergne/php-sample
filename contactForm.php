<?php 
//message form for contact page

if(isset($_POST['nameField']) && isset($_POST['emailField']) && isset($_POST['textField'])){
	$nameField = htmlentities($_POST['nameField']);
	$emailField = htmlentities($_POST['emailField']);
	$textField = htmlentities($_POST['textField']);
	//echo strlen($textField);
	if(!empty($nameField) && !empty($emailField) && !empty($textField)){
		if(substr($emailField, strpos($emailField, '@')+1)=='gmail.com' || substr($emailField, strpos($emailField, '@')+1)=='yahoo.com' || substr($emailField, strpos($emailField, '@')+1)=='hotmail.com' || 
		   substr($emailField, strpos($emailField, '@')+1)=='me.com'){	
			$handle = fopen('messages.txt','a');
			
			fwrite($handle,date("F j, Y, g:i a")."\r\n");
			fwrite($handle,'Name: ' .$nameField."\r\n");
			fwrite($handle,'Email: ' .$emailField."\r\n");
			fwrite($handle,'Message: ' .$textField."\r\n");
			fwrite($handle,"\r\n");
			fclose($handle);
			
			$to = 'Brian.Lavergne@gmail.com';
			$subject = 'Contact from submitted';
			$body = $nameField."\n".$textField;
			$headers = 'From: '.$emailField;
			
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Thanks for the feedback!";
		}
		else{
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Invalid Email.';
		}
	}
	else{
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All fields required.";
	}
}

?>
<html>
<head>`
<script language="javascript" type="text/javascript"> 
function textCounter(textField, showCountField) {
    var maxAmount = 250;
    if (textField.value.length > maxAmount) {
        textField.value = textField.value.substring(0, maxAmount);
	} else { 
        showCountField.value = maxAmount - textField.value.length;
	}
}

function emailCounter(emailField) {
    var maxAmount = 40;
    if (emailField.value.length > maxAmount) {
        emailField.value = emailField.value.substring(0, maxAmount);
	} 
}

function nameCounter(nameField) {
    var maxAmount = 30;
    if (nameField.value.length > maxAmount) {
        nameField.value = nameField.value.substring(0, maxAmount);
	} 
}
</script>
</head>
<body>

<form method="post" style="position: relative; left: 50px;">
	Name:<br><input type="text" name="nameField" style="-webkit-border-radius: 5px;" onkeydown="nameCounter(this.form.nameField);" onkeyup="nameCounter(this.form.nameField);"><br><br>
	Email:<br><input type="text" name="emailField" style="-webkit-border-radius: 5px;" onkeydown="emailCounter(this.form.emailField);" onkeyup="emailCounter(this.form.emailField);"><br><br>
	Message:<br><textarea style="resize: none; -webkit-border-radius: 5px;" name="textField" rows="6" cols="30" onkeydown="textCounter(this.form.textField, this.form.countDisplay);" onkeyup="textCounter(this.form.textField, this.form.countDisplay);" ></textarea><br>
	Characters Remaining:<input readonly="text" name="countDisplay" size="3" maxlength="3" value="250"/><br />
	<input type="submit" value="Submit" >
</form>
</body>
</html>