<!DOCTYPE html>
<!--RAKBIO ITEMS-->
<html lang="en">
	<head>
		
		<meta charset="UTF-8"/>
		<meta name="description" content="First Site"/>
		<meta name="keywords" content="Rak, Fat, Diablo 3, RPG,"/>
		<title>Rak and Fat's</title>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			body {
				background-color: black;
			}
		</style>
		<!--[if lt IE 9]>
<script src="dist/html5shiv.js"></script>
<![endif]-->
	</head>	
	<body >
		<div  id="wrapper">
			<header>
				<img src="images/Image_Header_Fire.jpg" alt="fireBanner" style="position: absolute;">
				<img id="banner" src="images/RakFatBanner.png" alt="banner">
				<ul id="nav"> 
					<li><a href="index.php">Home</a></li>
					<li><a href="#">Warrior</a></li>
					<li><a href="#">Merchant</a></li>
					<li><a href="Contacts.php">Contact</a></li>	
				</ul>
			</header><!--end header-->
			<div class="content clearfix" style="background-color: rgba(0, 10, 100, 0.37); opacity: .9;">
				<img id="leftBanner" src="images/RakBanner.png" alt="leftBanner">
				<img id="rightBanner" src="images/RakBanner.png" alt="rightBanner" style="float: right;">
				<img src="images/Image_witch_doctor.jpg" alt="WitchDoc"  width= "400"; height= "500"; style="position: absolute; right: 50px; top: 100px; opacity: .2; z-index: -1;">
				
				
				
					<img src="images/Image_RakInventory_General.jpg" alt="generalinv" height="300" width="350"/>
					<div style="width: 200px; position: relative; float: right; top: 230px; right: -10px;"><hr style="width: 100%; margin: 0; position: absolute; right: 30px;" /><p style="color: black; font-size: .8em;">Hover over item for description</p></div>
					
					<a class="thumbnail"><img src="images/Image_RakInventory_WepMask.jpg" alt="wepMask" style="position:absolute;left: 145px;top: -104px;width: 47px; height: 89px; opacity: 0;"/><span><img src="images/Image_RakInventory_Wep.jpg" alt="wepZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_Ring1Mask.jpg" alt="ringMask" style="position:absolute;left: 154px;top:-142px;width: 29px; height: 30px; opacity: 0;"/><span><img src="images/Image_RakInventory_Ring1.jpg" alt="ringZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_GlovesMask.jpg" alt="glovesMask" style="position:absolute;left: 145px;top: -209px;width: 46px; height: 59px; opacity: 0;"/><span><img src="images/Image_RakInventory_Gloves.jpg" alt="glovesZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_ShoulderMask.jpg" alt="shoulderMask" style="position:absolute;left: 164px;top: -289px;width: 46px; height: 69px; opacity: 0;"/><span><img src="images/Image_RakInventory_Shoulders.jpg" alt="shoulderZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_HelmMask.jpg" alt="helmMask" style="position:absolute;left: 222px; top: -303px; 96px; width: 46px; height: 53px; opacity: 0;"/><span><img src="images/Image_RakInventory_Helm.jpg" alt="helmZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_AmuletMask.jpg" alt="amuletMask" style="position:absolute;left: 282px; top: -264px; width: 38px; height: 35px; opacity: 0;"/><span><img src="images/Image_RakInventory_Amulet.jpg" alt="amuletZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_BracersMask.jpg" alt="bracersMask" style="position:absolute;left: 300px;top: -209px;width: 43px; height: 57px; opacity: 0;"/><span><img src="images/Image_RakInventory_Bracers.jpg" alt="bracersZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_Ring2Mask.jpg" alt="ring2Mask" style="position:absolute;left: 308px;top: -142px;width: 29px; height: 30px; opacity: 0;"/><span><img src="images/Image_RakInventory_Ring2.jpg" alt="ring2Zoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_ShieldMask.jpg" alt="shieldMask" style="position:absolute;left: 298px;top: -102px;200px;width: 47px; height: 87px; opacity: 0;"/><span><img src="images/Image_RakInventory_Shield.jpg" alt="shieldZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_BootsMask.jpg" alt="bootsMask" style="position:absolute;left: 222px;top: -74px;width: 46px; height: 59px; opacity: 0;"/><span><img src="images/Image_RakInventory_Boots.jpg" alt="bootsZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_PantsMask.jpg" alt="pantsMask" style="position:absolute;left: 223px;top: -138px;width: 45px; height: 59px; opacity: 0;"/><span><img src="images/Image_RakInventory_Pants.jpg" alt="pantsZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_BeltMask.jpg" alt="beltMask" style="position:absolute;left: 216px;top: -164px;width: 56px; opacity: 0;"/><span><img src="images/Image_RakInventory_Belt.jpg" alt="beltZoom" /></span></a>
					<a class="thumbnail"><img src="images/Image_RakInventory_ChestMask.jpg" alt="chestMask" style="position:absolute;left: 217px;top: -246px; width: 56px; opacity: 0;"/><span><img src="images/Image_RakInventory_Chest.jpg" alt="chestZoom" /></span></a>
				
				<a href="FatBio.php" title="Fat's Gear"><img src="images/image_fatface.png" alt="fatFace" style="float: left; margin: 60px 0 0 0;"></a>
				<a href="#" title="Rak's Gear"><img src="images/image_rakface.png" alt="rakFace" style="float: right; height: 150px; margin: 25px 15px 0 0;"></a>
			</div><!--end div-->
			<footer>
				<img src="images/Image_Footer_Flames.jpg" alt="fireFooterr" style="position: absolute; bottom: 0px;">
				<p>&copy; Webrokio 2012</p>
			</footer><!--end footer-->
		</div><!--END WRAPPER-->
	</body><!--end body--> 

	
</html>



					
					
					
					
					
					
					
					
					
					
					






