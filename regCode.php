<?php
//registration code
include 'connectInc.php';
$sid = session_id();
if($sid) {
    echo "Session exists!";
} else {
    session_start();
}


if(isset($_POST['nameField']) && isset($_POST['passField']) && isset($_POST['rePassField']) && isset($_POST['emailField']) && isset($_POST['accountTypeField']) && isset($_POST['questionField']) && isset($_POST['answerField'])){
	$nameField = htmlentities(strtolower($_POST['nameField']));
	$passField = htmlentities($_POST['passField']);
	$rePassField = htmlentities($_POST['rePassField']);
	$passFieldMd5 = md5($passField);
	$rePassFieldMd5 = md5($rePassField);
	$emailField = htmlentities(strtolower($_POST['emailField']));
	$accountTypeField = $_POST['accountTypeField'];
	$questionField = $_POST['questionField'];
	$answerField = htmlentities(strtolower($_POST['answerField']));
	$answerFieldMd5 = md5($answerField);	
	
	if(!empty($nameField) && !empty($passField) && !empty($rePassField) && !empty($emailField) && !empty($answerField)){
		$duplicate = false;	
		$queryNameCheck = "SELECT username FROM members";
		$queryEmailCheck = "SELECT email FROM members";		
		$queryNameCheckRun = mysql_query($queryNameCheck);
		$queryEmailCheckRun = mysql_query($queryEmailCheck);
		
		while($queryRow = mysql_fetch_assoc($queryNameCheckRun)){
			if($queryRow['username'] == $nameField){
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;username already being used.<br>';
				$duplicate = true;
				break;
			}
		}
		while($queryRow2 = mysql_fetch_assoc($queryEmailCheckRun)){
			if(trim($queryRow2['email']) == $emailField){	
				echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email already being used.';
				$duplicate = true;
				break;
			}
		}
		if($passFieldMd5 != $rePassFieldMd5){
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Passwords do not match. <br>';
			$duplicate = true;
		}
		if(!$duplicate){
		$query = "INSERT INTO members VALUES ('','".mysql_real_escape_string($nameField)."','".mysql_real_escape_string($passFieldMd5)."','".mysql_real_escape_string($accountTypeField)."','".mysql_real_escape_string($emailField)."', '".mysql_real_escape_string($questionField)."','".mysql_real_escape_string($answerFieldMd5)."','')";
		if($mysql_query = mysql_query($query)){
			echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Created!';
			mysql_close();
		}
		else{
			echo'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sorry, could not register!';
		}
	  }
	}
	else{
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All fields required!';
	}
}
?>


<html>
<head>
	<script language="javascript" type="text/javascript"> 
function passCounter(passField) {
    var maxAmount = 20;
    if (passField.value.length > maxAmount) {
        passField.value = passField.value.substring(0, maxAmount);
        
	} 
}

function rePassCounter(rePassField) {
    var maxAmount = 20;
    if (rePassField.value.length > maxAmount) {
        rePassField.value = rePassField.value.substring(0, maxAmount);
	} 
}

function emailCounter(emailField) {
    var maxAmount = 40;
    if (emailField.value.length > maxAmount) {
        emailField.value = emailField.value.substring(0, maxAmount);
	} 
}

function nameCounter(nameField) {
    var maxAmount = 30;  
    if (nameField.value.length > maxAmount) {
        nameField.value = nameField.value.substring(0, maxAmount);
	} 
}

function answerCounter(answerField) {
    var maxAmount = 20;  
    if (answerField.value.length > maxAmount) {
        answerField.value = answerField.value.substring(0, maxAmount);
	} 
}

</script>
</head>
<body>

<form method="post" style="position: relative; left: 50px;">
	Username:<br><input type="text" name="nameField" style="-webkit-border-radius: 5px;" value="<?php if(!empty($nameField)){ echo $nameField;} ?>" onkeydown="nameCounter(this.form.nameField);" onkeyup="nameCounter(this.form.textField);"><br><br>
	Password:<br><input type="password" name="passField" style="-webkit-border-radius: 5px;"  onkeydown="passCounter(this.form.passField);" onkeyup="passCounter(this.form.passField);"><br><br>
	Confirm Password:<br><input type="password" name="rePassField" style="-webkit-border-radius: 5px;" onkeydown="rePassCounter(this.form.rePassField);" onkeyup="rePassCounter(this.form.rePassField);"><br><br>
	Email:<br><input type="text" name="emailField" style="-webkit-border-radius: 5px;" value = "<?php if(isset($emailField)){ echo $emailField;} ?>" onkeydown="emailCounter(this.form.emailField);" onkeyup="emailCounter(this.form.emailField);"><br><br>
	
	<select name="accountTypeField">
	<option value="w">Warrior</option>
	<option value="m">Merchant</option>
	</select><br><br>
	Challenge Question:<br>
	<select name="questionField">
	<option value="q1">What city were you born in?</option>
	<option value="q2">what is the name of your sibling?</option>
	<option value="q3">what is your favorite food?</option>
	</select><br><br>
	Answer:<br><input type="text" name="answerField" style="-webkit-border-radius: 5px;" onkeydown="answerCounter(this.form.answerField);" onkeyup="answerCounter(this.form.answerField);">
	<br><br>
	<input type="submit" value="Submit" >
	
</form>
</body>
</html>
